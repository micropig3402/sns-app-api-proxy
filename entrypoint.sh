# Dockerで実行するコマンド

# shell scriptとして実行する
#!/bin/sh

# errorがあった場合に画面に表示する
set -e

# 環境変数を"/etc/nginx/default.conf.tpl"に埋め込み"/etc/nginx/conf.d/default.conf"として出力する
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# バックグラウンドで勝手に起動し続けないように指定
nginx -g 'daemon off;'